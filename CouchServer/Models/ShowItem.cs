﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CouchServer.Models
{
    public class ShowItem
    {

        // Common properties.
        public int ShowId { get; set; }
        public string FullName { get; set; }
        public string Title { get; set; }
        public string TitleShort { get; set; }
        public string TitleDetail { get; set; }
        public string Duration { get; set; }
        public string Origin { get; set; }
        public string OriginShort { get; set; }
        public double OriginLogoScale { get; set; }
        public byte[] ThumbnailByteArray { get; set; }
        public string ThumbnailPath { get; set; }
        public DateTime DateAdded { get; set; }
        public string DateBroadcast { get; set; }
        public string Format { get; set; }

        public int AgeInDays
        {
            get { return ((int)(DateTime.Now - DateAdded).TotalDays)+1; }
        }

    }

}
