﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace CouchServer.Managers
{
    public class LiveStreamManager
    {

        public static IConfiguration Configuration;

        public static string mVersionInfo;
        public static string mVideoPlayerExecutable;
        public static string mVideoPlayerArguments;
        public static string mUrlLiveStreamOrf1;
        public static string mUrlLiveStreamOrf2;
        public static string mUrlLiveStreamOrf3;
        public static string mUrlLiveStreamOrfSportPlus;
        

        public LiveStreamManager()
        {
            // Nothing to do yet.
        }

        public static async Task<int> StartLiveStreamById(int id)
        {

            try
            {

                string tmpLiveStreamUrlToStart = string.Empty;

                // Get URLs for known live streams.
                mVersionInfo = Configuration["AppSettings:Version"];
                mVideoPlayerExecutable = Configuration["AppSettings:PathVideoPlayer"];
                mVideoPlayerArguments = Configuration["AppSettings:ArgsVideoPlayer"];
                mUrlLiveStreamOrf1 = Configuration["AppSettings:UrlLiveStreamOrf1"];
                mUrlLiveStreamOrf2 = Configuration["AppSettings:UrlLiveStreamOrf2"];
                mUrlLiveStreamOrf3 = Configuration["AppSettings:UrlLiveStreamOrf3"];
                mUrlLiveStreamOrfSportPlus = Configuration["AppSettings:UrlLiveStreamOrfSportPlus"];

                // Build command line string for VLC.
                switch (id)
                {

                    case 1:
                        tmpLiveStreamUrlToStart = mUrlLiveStreamOrf1;
                        break;

                    case 2:
                        tmpLiveStreamUrlToStart = mUrlLiveStreamOrf2;
                        break;

                    case 3:
                        tmpLiveStreamUrlToStart = mUrlLiveStreamOrf3;
                        break;

                    case 4:
                        tmpLiveStreamUrlToStart = mUrlLiveStreamOrfSportPlus;
                        break;

                    default:
                        tmpLiveStreamUrlToStart = "http://www.example.com";
                        break;

                }

                // First stop running playback.
                ShowManager.StopPlayback();

                // Connect to VPN server.
                int tmp = await ConnectToVpnServer("orf");

                // Build process.
                var tmpProcess = new Process
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = mVideoPlayerExecutable,
                        Arguments = "\"" + tmpLiveStreamUrlToStart + "\"" + mVideoPlayerArguments
                    }
                };

                // Start process.
                tmpProcess.Start();

            }
            catch (Exception ex)
            {
                ShowManager.UpdateIssueStack(ex);
            }

            return 0;
        }

        private static async Task<int> ConnectToVpnServer(string countryToConnect)
        {

            string tmpArgsScheduledTask = string.Empty;

            try
            {

                // Build command line string .
                switch (countryToConnect)
                {

                    case "orf":
                        tmpArgsScheduledTask = "/run /tn ORF";
                        break;

                    default:
                        tmpArgsScheduledTask = "/run /tn ORF";
                        break;

                }

                // First stop running playback.
                ShowManager.StopPlayback();

                // Build process.
                var tmpProcess = new Process
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = @"C:\Windows\System32\schtasks.exe",
                        Arguments = tmpArgsScheduledTask
                    }
                };

                // Start process and wait for the connection.
                tmpProcess.Start();
                Task.Delay(5000).Wait();

            }
            catch (Exception ex)
            {
                ShowManager.UpdateIssueStack(ex);
            }

            return 0;
        }


    }

}
