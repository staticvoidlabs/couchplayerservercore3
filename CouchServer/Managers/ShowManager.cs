﻿using CouchServer.Models;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace CouchServer.Managers
{
    public class ShowManager
    {

        private static List<ShowItem> mShowRepo;
        private static List<string> mIssues;
        private static int mVideoId;
        public static IConfiguration Configuration;

        public static string mVersionInfo;
        public static string mSearchPathLocal;
        public static string mExtensionsToLookFor;
        public static string mVideoPlayerExecutable;
        public static string mVideoPlayerArguments;
        public static string mPathFfmpeg;
        public static string mPathFfprobe;
        public static string mArgsFfmpegThumbnail;
        public static string mArgsFfprobeDuration;
        public static string mThumbnailCacheDir;
        public static int mThumbnailByteArrayDelay;
        public static int mThumbnailDefaultPosition;

        public ShowManager()
        {
            // Nothing to do yet.
        }

        public static async void InitShowsManager()
        {
            // Init issue stack.
            mIssues = new List<string>();

            // Get values from app settings.
            mVersionInfo = Configuration["AppSettings:Version"];
            mSearchPathLocal = Configuration["AppSettings:SearchPathLocal"];
            mExtensionsToLookFor = Configuration["AppSettings:ExtensionsToLookFor"];
            mVideoPlayerExecutable = Configuration["AppSettings:PathVideoPlayer"];
            mVideoPlayerArguments = Configuration["AppSettings:ArgsVideoPlayer"];
            mPathFfmpeg = Configuration["AppSettings:PathFfmpeg"];
            mPathFfprobe = Configuration["AppSettings:PathFfprobe"];
            mArgsFfmpegThumbnail = Configuration["AppSettings:ArgsFfmpegThumbnail"];
            mArgsFfprobeDuration = Configuration["AppSettings:ArgsFfprobeDuration"];
            mThumbnailCacheDir = Configuration["AppSettings:ThumbnailCacheDir"];
            mThumbnailByteArrayDelay = int.Parse(Configuration["AppSettings:ThumbnailByteArrayDelay"]);
            mThumbnailDefaultPosition = int.Parse(Configuration["AppSettings:ThumbnailDefaultPosition"]);

            // Init show id counter.
            mVideoId = 0;

            // Instantiate shows repo if needed.
            if (mShowRepo == null)
            { 
                mShowRepo = new List<ShowItem>();
            }

            // Get shows in given location and fill repo.
            int tmpCounter = await PopulateShowsRepo();

            Console.WriteLine("Init > Added " + tmpCounter + " shows to repo.");
        }


        // PUBLIC methods.
        public static string GetAllShows()
        {
            string tmpShowItemsAsJson = JsonConvert.SerializeObject(mShowRepo);

            return tmpShowItemsAsJson;
        }

        public static string GetShowById(int showId)
        {
            string tmpShowItemsAsJson = JsonConvert.SerializeObject(mShowRepo.Where(s => s.ShowId == showId));

            return tmpShowItemsAsJson;
        }


        public static void StartPlayback(int showId)
        {

            try
            {

                // First stop running playback.
                StopPlayback();

                // Start playback.
                string tmpFileToPlayback = mShowRepo.Where(s => s.ShowId == showId).FirstOrDefault().FullName;

                var tmpProcess = new Process
                {
                    StartInfo = new ProcessStartInfo()
                    {
                        FileName = mVideoPlayerExecutable,
                        Arguments = "\"" + tmpFileToPlayback + "\"" + mVideoPlayerArguments
                    }
                };

                tmpProcess.Start();

            }
            catch (Exception ex)
            {
                UpdateIssueStack(ex);
            }
            
        }

        public static void StopPlayback()
        {

            // Kill all running vlc instances.
            try
            {
                foreach (Process proc in Process.GetProcessesByName("vlc"))
                {
                    proc.Kill();
                }
            }
            catch (Exception ex)
            {
                UpdateIssueStack(ex);
            }

        }

        public static async void DeleteShow(int showId)
        {

            // Delete given show from repo and share.
            try
            {

                // Delete selected file from disc.
                string[] tmpShowToDelete = Directory.GetFiles(mSearchPathLocal, mShowRepo.Where(s => s.ShowId == showId).First().Title);

                if (tmpShowToDelete.Length == 1)
                {
                    File.Delete(tmpShowToDelete[0]);
                }

                // Delete selected video from shows repo.
                mShowRepo.Remove(mShowRepo.Where(s => s.ShowId == showId).First());
            }
            catch (Exception ex)
            {
                UpdateIssueStack(ex);
            }

        }

        public static async Task<string> RefreshRepo()
        {

            string tmpShowItemsAsJson = string.Empty;
            int tmpCounter = 0;

            // Re-populate shows repo.
            try
            {
                mShowRepo = null;

                tmpCounter = await PopulateShowsRepo();
            }
            catch (Exception ex)
            {
                UpdateIssueStack(ex);
            }
            finally
            {
                tmpShowItemsAsJson = JsonConvert.SerializeObject(mShowRepo);
            }

            Console.WriteLine("Refreh > Added " + tmpCounter + " shows to repo.");

            return tmpShowItemsAsJson;
        }

        public static async Task<string> GetStatus()
        {

            string tmpServerStatus = string.Empty;

            try
            {

                // Build base info string.
                string tmpInfoBase = "Current server status:" + Environment.NewLine + Environment.NewLine;
                tmpInfoBase = tmpInfoBase + "  > Version: " + mVersionInfo + Environment.NewLine + Environment.NewLine;

                // Build show repo info strings.
                string tmpInfoShowsRepo = "  > Shows in repository: " + mShowRepo.Count() + Environment.NewLine;
                
                if (mShowRepo.Count > 0)
                {
                    tmpInfoShowsRepo = tmpInfoShowsRepo + Environment.NewLine;

                    foreach (var tmpShow in mShowRepo)
                    {
                        tmpInfoShowsRepo = tmpInfoShowsRepo + "    " + tmpShow.FullName + Environment.NewLine;
                    }

                    tmpInfoShowsRepo = tmpInfoShowsRepo + Environment.NewLine;
                }

                // Build issue stack info strings.
                string tmpInfoIssues = "  > Last warnings and error: ";
                
                if (mIssues.Count > 0)
                {
                    tmpInfoIssues = tmpInfoIssues + Environment.NewLine;

                    foreach (var tmpIssue in mIssues)
                    {
                        tmpInfoIssues = tmpInfoIssues + "  >    Issue > " + tmpIssue + Environment.NewLine;
                    }

                    tmpInfoIssues = tmpInfoIssues + Environment.NewLine;
                }
                else
                {
                    tmpInfoIssues = tmpInfoIssues + "none";
                }

                tmpServerStatus = tmpInfoBase + tmpInfoShowsRepo + tmpInfoIssues;
            }
            catch (Exception ex)
            {
                UpdateIssueStack(ex); 
            }

            return tmpServerStatus;
        }

        public static async Task<string> Shutdown()
        {

            string tmpServerResponseMessage = "Sending shutdown command..." + Environment.NewLine + Environment.NewLine;

            try
            {

                // Set up shutdown process.
                var processInfo = new ProcessStartInfo();
                processInfo.FileName = "shutdown";
                processInfo.Arguments = "/s";
                processInfo.CreateNoWindow = true;
                processInfo.UseShellExecute = false;
                processInfo.RedirectStandardOutput = true;

                var process = new Process();
                process.StartInfo = processInfo;

                // Start process async
                await Task.Run(() => process.Start());

                tmpServerResponseMessage = process.StandardOutput.ReadLine();

            }
            catch (Exception ex)
            {
                tmpServerResponseMessage = "Error sending shutdown command..." + Environment.NewLine + Environment.NewLine + ex.Message;
                UpdateIssueStack(ex);
            }

            return tmpServerResponseMessage;
        }

        public static async Task GenerateThumbnailsForShows()
        {

            try
            {

                // Generate thumbnail image for shows.
                foreach (var show in mShowRepo)
                {

                    string tmpThumbnailFullPath = mThumbnailCacheDir + show.ShowId + ".jpg";
                    int tmpThumbnailPosition = GetThumbnailPositionByDuration(show.Duration);

                    string tmpParameters = string.Format(mArgsFfmpegThumbnail, tmpThumbnailPosition, show.FullName, tmpThumbnailFullPath);

                    // Set up process.
                    var processInfo = new ProcessStartInfo();
                    processInfo.FileName = @mPathFfmpeg;
                    processInfo.Arguments = tmpParameters;
                    processInfo.CreateNoWindow = true;
                    processInfo.UseShellExecute = false;

                    var process = new Process();
                    process.StartInfo = processInfo;
                    //process.WaitForExit();

                    // Delete existing file
                    File.Delete(tmpThumbnailFullPath);
                    
                    // Start process async
                    await Task.Run(() => process.Start());

                    // Update shows repo.
                    show.ThumbnailPath = tmpThumbnailFullPath;

                    //Console.WriteLine("Thumbnail generated: " + show.Title);
                }

                // We need some time here to ensure images are ready.
                await Task.Delay(mThumbnailByteArrayDelay);
                
                // Generate byte arrays for images.
                foreach (var show in mShowRepo)
                {
                    show.ThumbnailByteArray = await GetByteArrayForImageFile(show.ThumbnailPath);
                    //Console.WriteLine("Byte array generated: " + show.Title);
                }

            }
            catch (Exception ex)
            {
                UpdateIssueStack(ex);
            }

        }

        public static async Task GetDurationForShows()
        {

            try
            {

                // Get duration for each show.
                foreach (var show in mShowRepo)
                {

                    string tmpParameters = string.Format(mArgsFfprobeDuration, show.FullName);

                    // Set up process.
                    var processInfo = new ProcessStartInfo();
                    processInfo.FileName = @mPathFfprobe;
                    processInfo.Arguments = tmpParameters;
                    processInfo.CreateNoWindow = true;
                    processInfo.UseShellExecute = false;
                    processInfo.RedirectStandardOutput = true;

                    var process = new Process();
                    process.StartInfo = processInfo;

                    // Start process async
                    await Task.Run(() => process.Start());

                    string tmpProcessOutput = process.StandardOutput.ReadLine();

                    // Update shows repo.

                    string[] tmpArray = tmpProcessOutput.Split(".");

                    if (tmpArray.Length > 0)
                    {
                        TimeSpan time = TimeSpan.FromSeconds(Convert.ToDouble(tmpArray[0]));
                        show.Duration = time.ToString(); ;
                    }

                    //Console.WriteLine("Duration: " + tmpProcessOutput + " / " + show.Duration);
                }

            }
            catch (Exception ex)
            {
                UpdateIssueStack(ex);
            }

        }

        public async static Task<byte[]> GetByteArrayForImageFile(string fileName)
        {

            byte[] tmpImageByteArray = new byte[1];

            try
            {

                if (!File.Exists(fileName))
                {
                    Console.WriteLine("Info > Waiting for file: " + fileName);
                    await Task.Delay(6000);
                }

                FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);

                using (BinaryReader reader = new BinaryReader(fileStream))
                {
                    tmpImageByteArray = new byte[reader.BaseStream.Length];

                    for (int i = 0; i < reader.BaseStream.Length; i++)
                    {
                        tmpImageByteArray[i] = reader.ReadByte();
                    }

                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("Error > File not ready: " + ex.Message);

                UpdateIssueStack(ex);
            }

            return tmpImageByteArray;
        }


        // PRIVATE methods.
        private static async Task<int> PopulateShowsRepo()
        {

            int tmpShowCounter = 0;
            List<ShowItem> tmpShows = new List<ShowItem>();

            try
            {

                // Get local search Path from appsettings.
                //string tmpSearchPathLocal = Configuration["AppSettings:SearchPathLocal"];

                // Get file extentions to look for from appsettings.
                string[] tmpExtensions = mExtensionsToLookFor.Split(',');

                // Search files in given path and filter them by extension.
                DirectoryInfo dInfo = new DirectoryInfo(mSearchPathLocal);

                FileInfo[] files =
                    dInfo.EnumerateFiles()
                         .Where(f => tmpExtensions.Contains(f.Extension.ToLower()))
                         .ToArray();

                // Populate shows repo.
                foreach (var file in files)
                {
                    ShowItem tmpNewShow = new ShowItem();

                    tmpNewShow.ShowId = GetNextShowId();
                    tmpNewShow.FullName = file.FullName;
                    tmpNewShow.Title = file.Name;
                    tmpNewShow.Origin = GetOrigin(tmpNewShow.Title);
                    tmpNewShow.TitleShort = GetTitleShort(ref tmpNewShow);
                    tmpNewShow.Format = file.Extension;
                    tmpNewShow.Duration = "0.00";
                    tmpNewShow.DateAdded = file.LastWriteTime;

                    tmpShows.Add(tmpNewShow);
                    tmpShowCounter++;

                    Console.WriteLine("Added show: " + tmpNewShow.Origin + " / " + tmpNewShow.TitleShort);
                }

                mShowRepo = tmpShows.OrderBy(s => s.AgeInDays).ToList();
            }

            catch (Exception ex)
            {
                //Console.WriteLine("ERROR: " + ex.Message);
                UpdateIssueStack(ex);
            }
            
            return tmpShowCounter;
        }

        private static int GetNextShowId()
        {
            mVideoId = mVideoId + 1;

            return mVideoId;
        }

        private static int GetThumbnailPositionByDuration(string duration)
        {
            
            int postition = -1;

            try
            {

                // Calculate thumbnail position by given duration.
                string[] tmpArray = duration.Split(":");

                if (tmpArray.Length > 2)
                {
                    int tmpHours = int.Parse(tmpArray[0]);
                    int tmpMinutes = int.Parse(tmpArray[1]);
                    int tmpSeconds = int.Parse(tmpArray[2]);

                    postition = ((tmpHours * 60 * 60) + (tmpMinutes * 60) + tmpSeconds) / 2;
                }
                else
                {
                    postition = mThumbnailDefaultPosition;
                }

            }
            catch (Exception ex)
            {
                UpdateIssueStack(ex);

                postition = mThumbnailDefaultPosition;
            }

            return postition;
        }


        // Set origin of show item.
        private static string GetOrigin(string filename)
        {
            try
            {

                if (filename.Substring(0, 10).Split("-").Length == 3)
                {
                    return "YouTV";
                }

                else if (filename.Contains("WALULIS."))
                {
                    return "WALULIS";
                }

                else if (filename.Contains("WALULIS DAILY."))
                {
                    return "WALULIS DAILY";
                }

                else if (filename.Contains("WALULIS DAILY TURBO."))
                {
                    return "WALULIS DAILY TURBO";
                }

                else if (filename.Contains("Game Two"))
                {
                    return "Game Two";
                }

                else if (filename.Contains("__GAMETWO"))
                {
                    return "Game Two TEMP";
                }

                else if (filename.Contains("__RBTV"))
                {
                    return "RBTV";
                }

                else if (filename.Contains("Best of Beans"))
                {
                    return "RBTV";
                }

                else if (filename.Contains("Das schaffst du nie!"))
                {
                    return "Das schaffst du nie!";
                }

                else if (filename.Contains("PULS Reportage"))
                {
                    return "PULS Reportage";
                }

                else
                {
                    return "unknown";
                }
            }
            catch (Exception ex)
            {
                //Console.WriteLine("ERROR: " + filename + " > " + ex.Message);

                UpdateIssueStack(ex);

                return "unknown";
            }
    
        }

        // Parse filename and extract short title.
        private static string GetTitleShort (ref ShowItem showItem)
        {
            string tmpTitleShort = string.Empty;

            try
            {

                // Step 1: Remove extension.
                string[] tmpArray1 = showItem.Title.Split(".");

                if (tmpArray1.Length > 1)
                {
                    tmpTitleShort = tmpArray1[0];
                }

                // Step 2: Remove brandings.
                switch (showItem.Origin)
                {
                    case "YouTV":
                        // Build broacast date info string.
                        string[] tmpArray8 = tmpTitleShort.Substring(0, 16).Split("_");
                        showItem.DateBroadcast = tmpArray8[0].Replace("-", "/") + " " + tmpArray8[1].Replace("-", ":") + " Uhr";

                        // Build short title and origin.
                        tmpTitleShort = tmpTitleShort.Substring(17, tmpTitleShort.Length - 17);
                        string[] tmpArray2 = tmpTitleShort.Split("_");
                        tmpTitleShort = tmpArray2[0];
                        showItem.Origin = "YouTV (" + tmpArray2[1] + ")";
                        showItem.OriginShort = "youtv";
                        showItem.OriginLogoScale = 0.8;
                        tmpTitleShort = tmpTitleShort.Replace("-", " ").Replace("_ ", "- ");
                        break;

                    case "WALULIS":
                        tmpTitleShort = tmpTitleShort.Substring(0, tmpTitleShort.LastIndexOf(" _ WALULIS")).Replace("_ ", " - ");
                        showItem.OriginShort = "walulis";
                        showItem.OriginLogoScale = 0.8;
                        break;

                    case "WALULIS DAILY":
                        tmpTitleShort = tmpTitleShort.Substring(0, tmpTitleShort.LastIndexOf(" _ WALULIS DAILY")).Replace("_ ", " - ");
                        showItem.OriginShort = "walulis";
                        showItem.OriginLogoScale = 0.8;
                        break;

                    case "WALULIS DAILY TURBO":
                        tmpTitleShort = tmpTitleShort.Substring(0, tmpTitleShort.LastIndexOf(" _ WALULIS DAILY TURBO")).Replace("_ ", " - ");
                        showItem.OriginShort = "walulis";
                        showItem.OriginLogoScale = 0.8;
                        break;

                    case "Game Two":
                        string[] tmpArray3 = tmpTitleShort.Split("_");
                        tmpTitleShort = tmpTitleShort.Substring(0, tmpTitleShort.LastIndexOf(tmpArray3[tmpArray3.Length - 1]) - 2) + " (" + tmpArray3[tmpArray3.Length - 1].TrimStart() + ")".Replace("_ ", " - ");
                        showItem.OriginShort = "gametwo";
                        showItem.OriginLogoScale = 1.2;
                        break;
                    
                    case "Game Two TEMP":
                        string[] tmpArray6 = tmpTitleShort.Split("__");
                        tmpTitleShort = tmpArray6[0].Replace("_ ", " - ");
                        showItem.Origin = "Game Two";
                        showItem.OriginShort = "gametwo";
                        showItem.OriginLogoScale = 1.2;
                        break;

                    case "RBTV":
                        tmpTitleShort = tmpTitleShort.Replace("__RBTV", "");
                        showItem.OriginShort = "rbtv";
                        showItem.OriginLogoScale = 1.5;
                        break;

                    case "Das schaffst du nie!":
                        string[] tmpArray4 = tmpTitleShort.Split(" __ ");
                        tmpTitleShort = tmpArray4[0];
                        showItem.OriginShort = "unknown";
                        showItem.OriginLogoScale = 1;
                        break;

                    case "PULS Reportage":
                        string[] tmpArray5 = tmpTitleShort.Split(" __ ");
                        tmpTitleShort = tmpArray5[0];
                        showItem.OriginShort = "unknown";
                        showItem.OriginLogoScale = 1;
                        break;

                    default:
                        tmpTitleShort = tmpArray1[0];
                        showItem.OriginShort = "unknown";
                        showItem.OriginLogoScale = 1;
                        break;

                }

            }
            catch (Exception ex)
            {
                //Console.WriteLine("ERROR: " + showItem.Title + " > " + ex.Message);

                UpdateIssueStack(ex);

                return tmpTitleShort;
            }

            return tmpTitleShort;
        }

        public static void UpdateIssueStack(Exception exception)
        {

            try
            {
                mIssues.Add(exception.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error updating issue stack:" + Environment.NewLine);
                Console.WriteLine(ex.Message);
            }

        }

    }

}
