﻿using CouchServer.Managers;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CouchServer.Services
{
    public class RepoService : IHostedService
    {

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            await Task.Run(ShowManager.InitShowsManager);
            await ShowManager.GetDurationForShows();
            await ShowManager.GenerateThumbnailsForShows();
        }

        public Task StopAsync(CancellationToken cancellationToken) => Task.CompletedTask;

    }

}
