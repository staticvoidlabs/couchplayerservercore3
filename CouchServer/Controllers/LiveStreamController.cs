﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CouchServer.Managers;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CouchServer.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class LiveStreamController : ControllerBase
    {

        // GET: api/livestream/connect/1
        [HttpGet]
        [Route("api/[controller]/[action]/{country}")]
        public async Task<string> Connect(int country)
        {

            //await ShowManager.Shutdown();

            return "livestream:action_connect:" + country.ToString();
        }

        // GET: api/livestream/disconnect
        [HttpGet]
        [Route("api/[controller]/[action]")]
        public async Task<string> Disconnect()
        {

            //await ShowManager.Shutdown();

            return "livestream:action_disconnect";
        }

        // GET: api/livestream/play/1
        [HttpGet]
        [Route("api/[controller]/[action]/{id}")]
        public string Play(int id)
        {

            return LiveStreamManager.StartLiveStreamById(id).ToString();
            //return "livestream:action_play:" + id;
        }

    }

}