﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CouchServer.Managers;
using CouchServer.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CouchServer.Controllers
{
    //[Route("api/[controller]")]
    [ApiController]
    public class ShowsController : ControllerBase
    {

        // GET: api/shows
        [HttpGet]
        [Route("api/[controller]")]
        public string Get()
        {

            return ShowManager.GetAllShows();
        }


        // GET: api/shows/5
        [HttpGet]
        [Route("api/[controller]/{id}")]
        public string Get(int id)
        {

            return ShowManager.GetShowById(id);
        }


        // GET: api/shows/play/123
        [HttpGet]
        [Route("api/[controller]/[action]/{id}")]
        public string Play(int id) 
        {

            ShowManager.StartPlayback(id);

            return "action:play:" + id.ToString();
        }

        // GET: api/shows/stop
        [HttpGet]
        [Route("api/[controller]/[action]")]
        public string Stop()
        {

            ShowManager.StopPlayback();

            return "action:stop";
        }

        // GET: api/shows/delete/123
        [HttpGet]
        [Route("api/[controller]/[action]/{id}")]
        public string Delete(int id)
        {

            ShowManager.DeleteShow(id);

            return "action:delete:" + id.ToString();
        }

        // GET: api/shows/refresh
        [HttpGet]
        [Route("api/[controller]/[action]")]
        public async Task<string> Refresh()
        {
            
            return await ShowManager.RefreshRepo();
        }

        // GET: api/shows/status
        [HttpGet]
        [Route("api/[controller]/[action]")]
        public async Task<string> Status()
        {

            return await ShowManager.GetStatus();
        }

        // GET: api/shows/shutdown
        [HttpGet]
        [Route("api/[controller]/[action]")]
        public async Task<string> Shutdown()
        {

            return await ShowManager.Shutdown();
        }

        /*
        // POST: api/Shows
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT: api/Shows/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
        */

    }

}
